<?php

namespace NNPro\LoggingBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nn_pro_logging');

        $rootNode
            ->children()
                ->scalarNode('object_manager')
                    ->cannotBeEmpty()
                    ->defaultValue('doctrine.odm.mongodb.document_manager')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
