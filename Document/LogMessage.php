<?php
/**
 * @package rentorder
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 30.10.13
 * @time 18:52
 */

namespace NNPro\LoggingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="NNPro\LoggingBundle\Repository\LogMessageRepository",
 *  collection={
 *      "name"="log_message",
 *      "capped"=true,
 *      "size"=100000000
 *  }
 * )
 */
class LogMessage
{
    /**
     * @var int
     *
     * @MongoDB\Id(strategy="INCREMENT")
     */
    private $id;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    private $message;

    /**
     * @var array
     *
     * @MongoDB\Hash
     */
    private $context = array();

    /**
     * @var int
     *
     * @MongoDB\Int
     * @MongoDB\Index(background=true)
     */
    private $level;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    private $levelName;

    /**
     * @var string
     *
     * @MongoDB\String
     * @MongoDB\Index(background=true)
     */
    private $channel;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     * @MongoDB\Index(background=true, order="desc")
     */
    private $datetime;

    /**
     * @var array
     *
     * @MongoDB\Hash
     */
    private $extra = array();

    /**
     * @var string
     *
     * @MongoDB\String
     */
    private $formatted;

    /**
     * @param array $rawData
     *
     * @return LogMessage
     */
    public static function fromRawData(array $rawData)
    {
        $message = new self();

        $message->setMessage($rawData['message'])
            ->setContext($rawData['context'])
            ->setLevel($rawData['level'])
            ->setLevelName($rawData['level_name'])
            ->setChannel($rawData['channel'])
            ->setDatetime($rawData['datetime'])
            ->setExtra($rawData['extra'])
            ->setFormatted($rawData['formatted'])
            ;

        return $message;
    }

    /**
     * @param string $channel
     *
     * @return LogMessage
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param array $context
     *
     * @return LogMessage
     */
    public function setContext(array $context = array())
    {
        $this->context = $context;
        return $this;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param \DateTime $datetime
     *
     * @return LogMessage
     */
    public function setDatetime(\DateTime $datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param array $extra
     *
     * @return LogMessage
     */
    public function setExtra(array $extra = array())
    {
        $this->extra = $extra;
        return $this;
    }

    /**
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param string $formatted
     *
     * @return LogMessage
     */
    public function setFormatted($formatted)
    {
        $this->formatted = $formatted;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormatted()
    {
        return $this->formatted;
    }

    /**
     * @param int $id
     *
     * @return LogMessage
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $level
     *
     * @return LogMessage
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $levelName
     *
     * @return LogMessage
     */
    public function setLevelName($levelName)
    {
        $this->levelName = $levelName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * @param string $message
     *
     * @return LogMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
} 