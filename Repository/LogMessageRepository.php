<?php
/**
 * @package rentorder
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 30.10.13
 * @time 18:53
 */

namespace NNPro\LoggingBundle\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;

class LogMessageRepository extends DocumentRepository
{
} 