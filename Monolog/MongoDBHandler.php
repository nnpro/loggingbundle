<?php
/**
 * @package rentorder
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 30.10.13
 * @time 18:39
 */

namespace NNPro\LoggingBundle\Monolog;


use Doctrine\ODM\MongoDB\DocumentManager;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use NNPro\LoggingBundle\Document\LogMessage;

class MongoDBHandler extends AbstractProcessingHandler
{
    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @param DocumentManager $dm
     * @param Logger $logger
     */
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * Writes the record down to the log of the implementing handler
     *
     * @param  array $record
     * @return void
     */
    protected function write(array $record)
    {
        $message = LogMessage::fromRawData($record);

        $this->dm->persist($message);
        $this->dm->flush();
    }
}